#About
       
This is PSR-3 logger extended implementation for ePN purposes

https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md

# Usage
- Create Transport
- Create Formatter
- Create Logger using Transport and Formatter
- Add Logger in LoggerAggregator
- Create Message
- Send Message through LoggerAggregator

NOTE: one LoggerAggregator may contains many Loggers.
You don't need create LoggerAggregator every time.

## Example
```
//for example get new logger aggregator
$loggerAggregator = new SimpleLoggerAggregator();
 
//create new ErrorMessage logger, in file for filebeat
$transport = new FileTransport($filePath);
$formatter = new FilebeatFormatter();
$logger = new ErrorMessageLogger($transport,$formatter);
$loggerAggregator->add($logger);
 
//creating message from gate
$factory = new MessageFactory();
$message = $factory->createExceptionMessage(new \Exception('hello'));
 
//send message through logger aggregator
$loggerAggregator->log($message);
 
//congratulations, you are done.
//now you can drink coffee(or something else) and be happy.
```

      \    /\
       )  ( ')
      (  /  )
       \(__)|