<?php

namespace Tests\functional\Transports;


use Sol\Logger\Transports\TcpTransport;

class TcpTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \FunctionalTester
	 */
	protected $tester;

	protected $listenSocket;
	protected $host = '127.0.0.1'; //localhost ipv6
	protected $port = 6001;


	public function _before() {
		$this->listenSocket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		socket_bind($this->listenSocket, $this->host, $this->port);
		socket_listen($this->listenSocket);
	}

	public function testSend() {
		$tcpLogstashTransport = new TcpTransport($this->host, $this->port);
		$tcpLogstashTransport->send('hello kitty');
		$client = socket_accept($this->listenSocket);
		$readedData = socket_read($client, 4096);
		unset($tcpLogstashTransport); //correct shutdown socket
		$this->tester->assertEquals('hello kitty' . "\n", $readedData);
	}

	public function _after() {
		socket_close($this->listenSocket);
	}
}