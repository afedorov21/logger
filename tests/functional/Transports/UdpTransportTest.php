<?php

namespace Tests\functional\Transports;


use Sol\Logger\Transports\UdpTransport;

class UdpTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \FunctionalTester
	 */
	protected $tester;

	protected $listenSocket;
	protected $host = '127.0.0.1'; //localhost ipv6
	protected $port = 6000;


	public function _before() {
		$this->listenSocket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
		socket_bind($this->listenSocket, $this->host, $this->port);
	}

	public function testSend() {
		$udpLogstashTransport = new UdpTransport($this->host, $this->port);
		$udpLogstashTransport->send('hello kitty');
		socket_recvfrom($this->listenSocket, $readedData, 4096, 0, $this->host, $this->port);
		unset($udpLogstashTransport); //correct shutdown socket
		$this->tester->assertEquals('hello kitty', $readedData);
	}

	public function _after() {
		socket_close($this->listenSocket);
	}
}