<?php

namespace Tests\functional\Transports;


use Sol\Logger\Transports\FileTransport;

class FileTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \FunctionalTester
	 */
	protected $tester;

	protected $testDir = '/tmp/FileTransportTest';
	protected $testLogFile = '/tmp/FileTransportTest/example.log';
	protected $testBadLogFile = '/tmp/FileTransportTest/unwrittable/example.log';

	public function _before() {
		mkdir($this->testDir);
	}

	public function testSetGoodFilePath() {
		$fileTransport = new FileTransport($this->testLogFile);
		$this->tester->assertTrue($fileTransport->getWritable());
	}

	public function testBadFilePath() {
		$fileTransport = new FileTransport($this->testBadLogFile);
		$this->tester->assertFalse($fileTransport->getWritable());
		$this->tester->assertFalse($fileTransport->send("hello"));
	}

	public function testSend() {
		$fileTransport = new FileTransport($this->testLogFile);
		$fileTransport->send("hello");
		$this->tester->assertFileExists($this->testLogFile);
		$this->tester->openFile($this->testLogFile);
		$this->tester->seeFileContentsEqual('hello' . PHP_EOL);

		//try to send in existing file with another transport
		$fileTransport2 = new FileTransport($this->testLogFile);
		$fileTransport2->send("hello2");
		$this->tester->openFile($this->testLogFile);
		$this->tester->seeFileContentsEqual('hello' . PHP_EOL . 'hello2' . PHP_EOL);
	}

	public function _after() {
		$this->tester->cleanDir($this->testDir);
		rmdir($this->testDir);
	}
}