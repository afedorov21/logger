<?php

namespace Tests\unit\Levels;

use Psr\Log\LogLevel;
use Sol\Logger\Helpers\LogLevelValidator;

class LogLevelTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testEmergencyLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::EMERGENCY));
	}

	public function testAlertLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::ALERT));
	}

	public function testCriticalLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::CRITICAL));
	}

	public function testLogLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::ERROR));
	}

	public function testWarningLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::WARNING));
	}

	public function testNoticeLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::NOTICE));
	}

	public function testInfoLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::INFO));
	}

	public function testDebugLevelValidation() {
		$this->tester->assertTrue(LogLevelValidator::isValid(LogLevel::DEBUG));
	}

	public function testIncorrectLevelValidation() {
		$this->tester->assertFalse(LogLevelValidator::isValid('random_bad_string'));
	}
}