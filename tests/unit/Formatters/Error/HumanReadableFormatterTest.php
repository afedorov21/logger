<?php

namespace Tests\unit\Formatters;

use Codeception\Util\Stub;
use Sol\Logger\Formatters\Error\HumanReadableFormatter;

class HumanReadableFormatterTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected $errorMessage;
	protected $errorMessageData;
	protected $LogLevel;

	public function _before() {
		$this->errorMessageData = array(
			'message' => 'hello',
			'file' => 'example.php',
			'line' => 3,
			'trace' => 'some_trace',
			'timestamp' => time(),
			'remoteAddr' => '127.0.0.1',
		);
		$this->errorMessage = Stub::make(
			'Sol\Logger\Messages\Error\ExceptionMessage',
			$this->errorMessageData
		);
		$this->LogLevel = 'example_error_level';
	}

	public function testFormat() {
		$humanReadableFormatter = new HumanReadableFormatter();
		$this->tester->assertEquals($this->buildExpectedMessage($this->LogLevel, $this->errorMessage), $humanReadableFormatter->format($this->LogLevel, $this->errorMessage));
	}

	protected function buildExpectedMessage($level, $message) {
		$buf = '';
		$buf .= date('r', $message->getTimestamp());
		$buf .= ' - ';
		$buf .= $message->getRemoteAddr() ?? '~';
		$buf .= ' - ' . php_uname('n');
		$buf .= ' - ' . $level;
		$buf .= ' - ' . $message->getMessage()
			. " on " . $message->getFile() . ':' . $message->getLine()
			. "\nTrace: " . $message->getTrace()
			. "\n";
		return $buf;
	}
}