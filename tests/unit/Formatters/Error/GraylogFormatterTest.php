<?php

namespace Tests\unit\Formatters;

use Codeception\Util\Stub;
use Sol\Logger\Formatters\Error\GraylogFormatter;

class GraylogFormatterTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $errorMessage;
    protected $errorMessageData;
    protected $LogLevel;

    public function _before() {
        $this->errorMessageData = array(
            'message' => 'hello',
            'file' => 'example.php',
            'line' => 3,
            'trace' => 'some_trace',
            'timestamp' => time(),
            'remoteAddr' => '127.0.0.1',
        );
        $this->errorMessage = Stub::make(
            'Sol\Logger\Messages\Error\ExceptionMessage',
            $this->errorMessageData
        );
        $this->LogLevel = 'example_error_level';
    }

    public function testFormat() {
        $graylogFormatter = new GraylogFormatter();
        $this->tester->assertEquals($this->buildExpectedMessage($this->LogLevel, $this->errorMessage), $graylogFormatter->format($this->LogLevel, $this->errorMessage));
    }

    protected function buildExpectedMessage($level, $message) {
        $buf = [];
        $buf['version'] = '1.1';
        $buf['host'] = 'back';
        $buf['short_message'] = $message->getMessage();
        $buf['app__date'] = \date(DATE_RFC3339, $message->getTimestamp());
        $buf['app__remote_addr'] = $message->getRemoteAddr() ?? '~';
        $buf['app__file'] = $message->getFile();
        $buf['app__line'] = $message->getLine();
        $buf['app__level'] = $level;
        $buf['app__message'] = $message->getMessage();
        $buf['app__trace'] = $message->getTrace();
        return \json_encode($buf);
    }
}