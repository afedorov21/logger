<?php

namespace Tests\unit\Levels;

use Codeception\Util\Stub;
use Psr\Log\LogLevel;
use Sol\Logger\Loggers\ErrorMessageLogger;
use Sol\Logger\Formatters\Error\ErrorFormatterInterface;
use Sol\Logger\Messages\Error\ErrorMessageInterface;
use Sol\Logger\Transports\TransportInterface;
use Sol\Logger\Exceptions\InvalidArgumentException;

class ErrorMessageLoggerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected $transport;
	protected $formatter;

	public function _before() {
		$this->transport = Stub::makeEmpty(
			TransportInterface::class,
			array(
				'send' => function ($data) {
					return $data;
				}
			)
		);
		$this->formatter = Stub::makeEmpty(
			ErrorFormatterInterface::class,
			array(
				'format' => function ($level, $message) {
					return "{$level}_{$message}";
				}
			)
		);
	}

	public function testInvalidArgumentException() {
		$errorMessageLogger = new ErrorMessageLogger($this->transport, $this->formatter);
		$this->tester->expectException(InvalidArgumentException::class, function () use ($errorMessageLogger) {
			$errorMessageLogger->log('bad_level', 'hello');
		});
	}

	public function testLog() {
		$errorMessageLogger = new ErrorMessageLogger($this->transport, $this->formatter);
		$message = Stub::makeEmpty(
			ErrorMessageInterface::class,
			array(
				'__toString' => 'message_text'
			)
		);
		$this->tester->assertEquals('emergency_message_text', $errorMessageLogger->log('emergency', $message));
		$this->tester->assertEquals('alert_message_text', $errorMessageLogger->log('alert', $message));
		$this->tester->assertEquals('critical_message_text', $errorMessageLogger->log('critical', $message));
		$this->tester->assertEquals('error_message_text', $errorMessageLogger->log('error', $message));
		$this->tester->assertEquals('warning_message_text', $errorMessageLogger->log('warning', $message));
		$this->tester->assertEquals('notice_message_text', $errorMessageLogger->log('notice', $message));
		$this->tester->assertEquals('info_message_text', $errorMessageLogger->log('info', $message));
		$this->tester->assertEquals('debug_message_text', $errorMessageLogger->log('debug', $message));
	}

	public function testLogInvalidType() {
		$errorMessageLogger = new ErrorMessageLogger($this->transport, $this->formatter);
		$this->tester->assertFalse($errorMessageLogger->log(LogLevel::DEBUG, 'hello'));
	}
}