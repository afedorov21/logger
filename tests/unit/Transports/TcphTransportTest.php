<?php

namespace Tests\unit\Messages\Error;

use Codeception\Util\Stub;
use Sol\Logger\Transports\TcpTransport;

class TcphTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testGetters() {
		$tcpLogstashTransport = Stub::make(
			TcpTransport::class,
			array(
				'host' => '127.0.0.1',
				'port' => 123,
				'__destruct' => function () {
				},
			)
		);
		$this->tester->assertEquals('127.0.0.1', $tcpLogstashTransport->getHost());
		$this->tester->assertEquals(123, $tcpLogstashTransport->getPort());
	}
}