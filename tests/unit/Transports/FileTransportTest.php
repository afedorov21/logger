<?php

namespace Tests\unit\Messages\Error;

use Codeception\Util\Stub;
use Sol\Logger\Transports\FileTransport;

class FileTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testGetFilePath() {
		$fileTransport = Stub::make(
			FileTransport::class,
			array(
				'filePath' => 'example'
			)
		);
		$this->tester->assertEquals('example', $fileTransport->getFilePath());
	}
}