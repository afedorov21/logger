<?php

namespace Tests\unit\Messages\Error;

use Codeception\Util\Stub;
use Sol\Logger\Transports\UdpTransport;

class UdpTransportTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testGetters() {
		$udpLogstashTransport = Stub::make(
			UdpTransport::class,
			array(
				'host' => '127.0.0.1',
				'port' => 123,
				'__destruct' => function () {
				},
			)
		);
		$this->tester->assertEquals('127.0.0.1', $udpLogstashTransport->getHost());
		$this->tester->assertEquals(123, $udpLogstashTransport->getPort());
	}
}