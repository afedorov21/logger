<?php

namespace Tests\unit\Formatters;

use Codeception\Util\Stub;
use Sol\Logger\Aggregators\SimpleLoggerAggregator;
use Psr\Log\LoggerInterface;

class SimpleLoggerAggregatorTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected $loggers;
	protected $loggersLog;
	protected $loggerAggregator;
    protected $loggersLogResults;

	public function _before() {
		$this->loggersLogResults[0] = 'logger 0';
		$this->loggers[0] = Stub::makeEmpty(
			LoggerInterface::class,
			array(
				'log' => function () {
					return $this->loggersLog[0];
				},
			)
		);

		$this->loggersLogResults[1] = 'logger 1';
		$this->loggers[1] = Stub::makeEmpty(
			LoggerInterface::class,
			array(
				'log' => function () {
					return $this->loggersLog[1];
				},
			)
		);
	}

	public function testAddLogger() {
		$simpleLoggerAggregator = new SimpleLoggerAggregator();
		$logger = Stub::makeEmpty(
			LoggerInterface::class
		);
		$this->tester->assertEquals(0, $simpleLoggerAggregator->add($logger));
		$this->tester->assertEquals(1, $simpleLoggerAggregator->add($logger));
	}

	public function testLog() {
		$simpleLoggerAggregator = new SimpleLoggerAggregator();

		$logger0 = Stub::makeEmpty(
			LoggerInterface::class,
			array(
				'log' => function () {
					return 'logger 0';
				},
			)
		);
		$logger1 = Stub::makeEmpty(
			LoggerInterface::class,
			array(
				'log' => function () {
					return 'logger 1';
				},
			)
		);

		$simpleLoggerAggregator->add($logger0);
		$simpleLoggerAggregator->add($logger1);

		$results = $simpleLoggerAggregator->log('debug', 'lol');

		$this->tester->assertEquals('logger 0', $results[0]);
		$this->tester->assertEquals('logger 1', $results[1]);
	}

	public function testLogWithoutLoggers() {
		$simpleLoggerAggregator = new SimpleLoggerAggregator();
		$this->tester->assertEquals([], $simpleLoggerAggregator->log('debug', 'lol'));
	}
}