<?php

namespace Tests\unit\Messages\Error;

use Sol\Logger\Messages\Error\StringMessage;

class StringMessageTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testConstructor() {
		$message = 'hello {who}';
		$data = array(
			'who' => 'kitty',
			'file' => 'example.php',
			'line' => 2,
			'trace' => 'some trace...',
			'remoteAddr' => '127.0.0.1',
			// -rand just for difference with real timestamp
			'timestamp' => time() - rand(100, 86400),
			'requestId' => '123qwe',
		);
		$stringMessage = new StringMessage($message, $data);

		$this->tester->assertEquals($data['line'], $stringMessage->getLine());
		$this->tester->assertEquals($data['file'], $stringMessage->getFile());
		$this->tester->assertEquals($data['trace'], $stringMessage->getTrace());
		$this->tester->assertEquals('hello kitty', $stringMessage->getMessage());
		$this->tester->assertEquals($data['remoteAddr'], $stringMessage->getRemoteAddr());
		$this->tester->assertEquals($data['timestamp'], $stringMessage->getTimestamp());
	}

	public function testConstructorWithoutRemoteAddrAndTimestamp() {
		$message = 'hello {who}';
		$data = array(
			'who' => 'kitty',
			'file' => 'example.php',
			'line' => 2,
			'trace' => 'some trace...',
		);
		$timestamp_now = time();
		$stringMessage = new StringMessage($message, $data);
		$this->tester->assertEmpty($stringMessage->getRemoteAddr());
		$this->tester->assertEquals($timestamp_now, $stringMessage->getTimestamp());
	}
}