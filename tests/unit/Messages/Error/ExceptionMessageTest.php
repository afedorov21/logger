<?php

namespace Tests\unit\Messages\Error;

use Sol\Logger\Messages\Error\ExceptionMessage;

class ExceptionMessageTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testConstructor() {
		$exception = new \Exception('hello kitty');
		$data = array(
			'remoteAddr' => '127.0.0.1',
			'timestamp' => time() - rand(100, 86400), // -rand just for difference with real timestamp
			'requestId' => '123qwe'
		);
		$exceptionMessage = new ExceptionMessage($exception, $data);

		$this->tester->assertEquals($exception->getLine(), $exceptionMessage->getLine());
		$this->tester->assertEquals($exception->getFile(), $exceptionMessage->getFile());
		$this->tester->assertEquals($exception->getTraceAsString(), $exceptionMessage->getTrace());
		$this->tester->assertEquals($exception->getMessage(), $exceptionMessage->getMessage());
		$this->tester->assertEquals($data['remoteAddr'], $exceptionMessage->getRemoteAddr());
		$this->tester->assertEquals($data['timestamp'], $exceptionMessage->getTimestamp());
	}

	public function testConstructorWithEmptyDataArray() {
		$exception = new \Exception('hello kitty');
		$timestamp_now = time();
		$exceptionMessage = new ExceptionMessage($exception);
		$this->tester->assertEmpty($exceptionMessage->getRemoteAddr());
		$this->tester->assertEquals($timestamp_now, $exceptionMessage->getTimestamp());
	}
}