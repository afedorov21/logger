<?php

namespace Tests\unit\Messages;

use Sol\Logger\Messages\Error\ErrorMessage;
use Sol\Logger\Messages\Error\ExceptionMessage;
use Sol\Logger\Messages\MessageFactory;

class MessageFactoryTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testCreateMessageType() {
		$messageFactory = new MessageFactory();
		$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
		$_SERVER['REQUEST_ID'] = '123';
		$exceptionMessage = $messageFactory->createExceptionMessage(new \Exception('hello'), array());
		$this->tester->assertInstanceOf(ExceptionMessage::class, $exceptionMessage);
		$this->tester->assertEquals('127.0.0.1', $exceptionMessage->getRemoteAddr());
	}
}