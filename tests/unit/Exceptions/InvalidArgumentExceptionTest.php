<?php

namespace Tests\unit\Formatters;

use Codeception\Util\Stub;
use Sol\Logger\Exceptions\InvalidArgumentException;

class InvalidArgumentExceptionTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testExtendsBaseClass() {
		$invalidArgumentException = new InvalidArgumentException();
		$this->tester->assertInstanceOf(\InvalidArgumentException::class, $invalidArgumentException);
	}
}