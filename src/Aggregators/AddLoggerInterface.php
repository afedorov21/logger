<?php


namespace Sol\Logger\Aggregators;


use Psr\Log\LoggerInterface;

interface AddLoggerInterface {

	public function add(LoggerInterface $logger);
}