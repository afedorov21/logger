<?php
declare(strict_types=1);

namespace Sol\Logger\Aggregators;

use Psr\Log\LoggerInterface;


/**
 * Interface LoggerAggregatorInterface
 * @package Sol\Logger\Aggregators
 */
interface LoggerAggregatorInterface  extends LoggerInterface, AddLoggerInterface {

}