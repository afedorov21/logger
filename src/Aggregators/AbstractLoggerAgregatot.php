<?php


namespace Sol\Logger\Aggregators;


use Sol\Logger\Loggers\AbstractLogger;
use Psr\Log\LoggerInterface;

abstract class AbstractLoggerAgregatot extends AbstractLogger implements LoggerAggregatorInterface {

    /**
     * @var null|LoggerInterface[]
     */
    protected $loggers;

    /**
     * Add logger in Aggregator
     * @param LoggerInterface $logger
     * @return int - logger id
     */
    public function add(LoggerInterface $logger): int {
        $this->loggers[] = $logger;
        return \count($this->loggers) - 1;
    }

    public abstract function log($level, $message, array $context = []);
}