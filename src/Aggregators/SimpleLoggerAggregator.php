<?php
declare(strict_types=1);

namespace Sol\Logger\Aggregators;

use Psr\Log\LoggerInterface;

/**
 * Logger aggregator just call log function in added loggers
 * Class SimpleLoggerAggregator
 * @package Sol\Logger\Aggregators
 */
class SimpleLoggerAggregator extends AbstractLoggerAgregatot implements LoggerAggregatorInterface {

	/**
	 * Log message using loggers.
	 * @param string $level - log level
	 * @param mixed $message - message for logging
	 * @param array $context - context data
	 * @return array - results for each logger by id
	 */
	public function log($level, $message, array $context = []) {
		$result = [];
		if (null === $this->loggers) {
			return $result;
		}
		foreach ($this->loggers as $key => $logger) {
			$result[$key] = $logger->log($level, $message, $context);
		}
		return $result;
	}
}