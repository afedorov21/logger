<?php
declare(strict_types=1);

namespace Sol\Logger\Exceptions;

/**
 * The InvalidArgumentException on bad level
 * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
 * Class InvalidArgumentException
 * @package Sol\Logger\Exceptions
 */
class InvalidArgumentException extends \InvalidArgumentException {
}