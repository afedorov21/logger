<?php

declare(strict_types=1);

namespace Sol\Logger\Transports;

/**
 * Transport for writing messages in file
 * Class FileTransport
 * @package Sol\Logger\Transports
 */
class FileTransport implements TransportInterface {
	/**
	 * Path to file
	 * @var string
	 */
	protected $filePath;

	/**
	 * File is writable
	 * @var bool
	 */
	protected $writable = false;

	/**
	 * FileTransport constructor.
	 * @param string $filePath
	 */
	public function __construct(string $filePath) {
		$this->setFilePath($filePath);
	}

	/**
	 * Get path to file
	 * @return string - path to file
	 */
	public function getFilePath(): string {
		return $this->filePath;
	}

	/**
	 * Get writable
	 * @return bool
	 */
	public function getWritable(): bool {
		return $this->writable;
	}

	/**
	 * Set file for writing
	 * @param string $filePath
	 * @return bool
	 */
	protected function setFilePath(string $filePath) {
		if ($filePath && (file_exists(dirname($filePath)) && is_writable($filePath))) {
			$this->writable = true;
		}
		if ($filePath && (!file_exists($filePath) && is_writable(dirname($filePath)))) {
			$this->writable = true;
		}
		$this->filePath = $filePath;
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function send(string $data) {
	    if ($this->writable) {
			return file_put_contents($this->getFilePath(), $data . PHP_EOL, FILE_APPEND);
		}
		return false;
	}
}