<?php
declare(strict_types=1);

namespace Sol\Logger\Transports;
use Solbianca\VarDumper\VarDumper;

/**
 * Transport for writing messages through UDP to Logshtash(ELK)
 * Class UdpTransport
 * @package Sol\Logger\Transports
 */
class UdpTransport implements TransportInterface {
	/**
	 * Host for write
	 * @var string
	 */
	protected $host;
	/**
	 * Port for write
	 * @var int
	 */
	protected $port;

	/**
	 * Socket
	 * @var resource
	 */
	protected $socket;

	/**
	 * UdpTransport constructor.
	 * @param string $host - host for writing data
	 * @param int $port - port for writing data
	 */
	public function __construct(string $host,int $port) {
		$this->host = $host;
		$this->port = $port;
		$this->socket = \socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
	}

	/**
	 * Get current host
	 * @return string - host
	 */
	public function getHost(): string {
		return $this->host;
	}

	/**
	 * Get current port
	 * @return int - port
	 */
	public function getPort(): int {
		return $this->port;
	}

	/**
	 * {@inheritdoc}
	 */
	public function send(string $data) {
		return \socket_sendto($this->socket, $data, strlen($data), 0, $this->host, $this->port);
	}

	/**
	 * Correct close socket
	 */
	public function __destruct() {
		\socket_close($this->socket);
	}
}