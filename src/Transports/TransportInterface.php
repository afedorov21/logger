<?php
declare(strict_types=1);

namespace Sol\Logger\Transports;

/**
 * Interface for transports
 * Interface TransportInterface
 * @package Sol\Logger\Transports
 */
interface TransportInterface {
	/**
	 * Send data
	 * @param string $data
	 * @return mixed
	 */
	public function send(string $data);
}