<?php
declare(strict_types=1);

namespace Sol\Logger\Transports;

/**
 * Transport for writing messages through TCP to Logshtash(ELK)
 * Class TcpTransport
 * @package Sol\Logger\Transports
 */
class TcpTransport implements TransportInterface {
	/**
	 * Host for write
	 * @var string
	 */
	protected $host;
	/**
	 * Port for write
	 * @var int
	 */
	protected $port;

	/**
	 * Socket
	 * @var resource
	 */
	protected $socket;

	/**
	 * TcpTransport constructor.
	 * @param string $host - host for writing data
	 * @param int $port - port for writing data
	 */
	public function __construct(string $host, int $port) {
		$this->host = $host;
		$this->port = $port;
		$this->socket = \socket_create(AF_INET, SOCK_STREAM, 0);
		\socket_connect($this->socket, $this->host, $this->port);
	}

	/**
	 * Get current host
	 * @return string - host
	 */
	public function getHost(): string {
		return $this->host;
	}

	/**
	 * Get current port
	 * @return int - port
	 */
	public function getPort(): int {
		return $this->port;
	}

	/**
	 * {@inheritdoc}
	 */
	public function send(string $data) {
		$data .= "\n";
		try {
            return \socket_write($this->socket, $data);
        } catch (\Throwable $e) {
		    return false;
        }
	}

	/**
	 * Correct close socket
	 */
	public function __destruct() {
		\socket_close($this->socket);
	}
}