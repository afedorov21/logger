<?php
declare(strict_types=1);

namespace Sol\Logger\Loggers;

use Sol\Logger\Formatters\Error\ErrorFormatterInterface;
use Sol\Logger\Messages\Error\ErrorMessageInterface;
use Sol\Logger\Transports\TransportInterface;

/**
 * Logger for ErrorMessageInterface messages only
 * Class ErrorMessageLogger
 * @package Sol\Logger\Loggers
 */
class ErrorMessageLogger extends AbstractLogger {
	/**
	 * ErrorMessageLogger constructor.
	 * @param TransportInterface $transport - transport for logger
	 * @param ErrorFormatterInterface $formatter - formatter for logger
	 */
	public function __construct(TransportInterface $transport, ErrorFormatterInterface $formatter) {
		$this->transport = $transport;
		$this->formatter = $formatter;
	}

	/**
	 * {@inheritdoc}
	 */
	public function log($level, $message, array $context = array()) {
		$this->validateLevelOrException($level);
		/**
		 * Implementors MAY have special handling for the passed objects.
		 * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
		 * log only ErrorMessageInterface messages
		 **/
		if ($message instanceof ErrorMessageInterface) {
			return $this->transport->send($this->formatter->format($level, $message));
		}
		return false;
	}
}