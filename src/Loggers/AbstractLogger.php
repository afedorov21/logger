<?php

declare(strict_types=1);

namespace Sol\Logger\Loggers;

use Psr\Log\LoggerInterface;
use Sol\Logger\Exceptions\InvalidArgumentException;
use Sol\Logger\Formatters\Error\ErrorFormatterInterface;
use Sol\Logger\Helpers\LogLevelValidator;
use Sol\Logger\Transports\TransportInterface;


/**
 * Class AbstractLogger - Base class for loggers
 * @package Sol\Logger\Loggers
 */
abstract class AbstractLogger extends \Psr\Log\AbstractLogger implements LoggerInterface {

    /**
	 * Formatter for logger
     *
	 * @var ErrorFormatterInterface - or OtherFormatter
	 */

	protected $formatter;
	/**
	 * Transport for logger
     *
	 * @var TransportInterface
	 */
	protected $transport;


    /**
     * All loggers need throw InvalidArgumentException if level is not valid, PSR-3
     *
     * @param string $level
     * @return bool
     * @throws \ReflectionException
     */
    protected function validateLevel(string $level) {
		if (!LogLevelValidator::isValid($level)) {
			return false;
		}
		return true;
	}

    /**
     * All loggers need throw InvalidArgumentException if level is not valid, PSR-3
     *
     * @param string $level
     * @return bool
     * @throws \ReflectionException
     */
	protected function validateLevelOrException(string $level) {
        if (!$this->validateLevel($level)) {
            throw new InvalidArgumentException();
        }
        return true;
    }
}