<?php

namespace Sol\Logger\Helpers;


use Psr\Log\LogLevel;

class LogLevelValidator
{
    /**
     * Validate log level
     *
     * @param string $level
     * @return bool
     * @throws \ReflectionException
     */
    public static function isValid(string $level):bool {
        $reflection = new \ReflectionClass(LogLevel::class);
        return \in_array($level, $reflection->getConstants());
    }
}