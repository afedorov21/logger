<?php
declare(strict_types=1);

namespace Sol\Logger\Messages\Error;

/**
 * Wrapper for create error messages from string
 * Class StringMessage
 * @package Sol\Logger\Messages\Error
 */
class StringMessage extends AbstractErrorMessage {
	/**
	 * StringMessage constructor.
	 * @param string $message
	 * @param array $data - requestId,remoteAddr,timestamp,file,line,trace
	 */
	public function __construct(string $message, array $data = []) {
		$this->setMessage($message, $data);
	}

	/**
	 * Set message
	 * @param string $message
	 * @param array $data - requestId,remoteAddr,timestamp,file,line,trace
	 * @return bool
	 */
	protected function setMessage(string $message, array $data = []): bool {
		$this->message = $this->interpolate($message, $data);
		$this->file = $data['file'] ?? '';
		$this->line = $data['line'] ?? 0;
		$this->trace = $data['trace'] ?? '';
		$this->parseDefaultParams($data);

		return true;
	}

	/**
	 * Replace message params {$k} to context variables
	 * for example $message = "hello {who}" , $data = array('who'=>'kitty'), result is "hello kitty"
	 * @param string $message
	 * @param array $data
	 * @return string - message with context replacements
	 */
	protected function interpolate(string $message, array $data = []): string {
		$replace = array();
		foreach ($data as $k => $v) {
			$replace['{' . $k . '}'] = $v;
		}
		return \strtr((string)$message, $replace);
	}
}