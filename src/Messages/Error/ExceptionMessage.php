<?php
declare(strict_types=1);

namespace Sol\Logger\Messages\Error;
/**
 * Wrapper for create error messages from Exception
 * Class ExceptionMessage
 * @package Sol\Logger\Messages\Error
 */
class ExceptionMessage extends AbstractErrorMessage {
	/**
	 * ExceptionMessage constructor.
	 * @param \Exception $message
	 * @param array $data - requestId,remoteAddr,timestamp
	 */
	public function __construct(\Exception $message, array $data = []) {
		$this->setMessage($message, $data);
	}

	/**
	 * Set error message
	 * @param \Exception $message
	 * @param array $data - requestId,remoteAddr,timestamp
	 * @return bool
	 */
	protected function setMessage(\Exception $message, array $data): bool {
		$this->message = $message->getMessage();
		$this->file = $message->getFile();
		$this->line = $message->getLine();
		$this->trace = $message->getTraceAsString();
		$this->parseDefaultParams($data);
		return true;
	}
}