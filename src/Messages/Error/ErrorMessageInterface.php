<?php
declare(strict_types=1);

namespace Sol\Logger\Messages\Error;
/**
 * Interface for error messages
 * Interface ErrorMessageInterface
 * @package Sol\Logger\Messages\Error
 */
interface ErrorMessageInterface {
	/**
	 * Get error message
	 * @return string - message
	 */
	public function getMessage(): string;

	/**
	 * Get error file
	 * @return string - file path
	 */
	public function getFile(): string;

	/**
	 * Get error line
	 * @return int - line
	 */
	public function getLine(): int;

	/**
	 * Get error trace
	 * @return string - trace
	 */
	public function getTrace(): string;

	/**
	 * Get error timestamp
	 * @return int - timestamp
	 */
	public function getTimestamp(): int;

	/**
	 * get error remote IP|addr
	 * @return string - IP|addr
	 */
	public function getRemoteAddr(): string;

	/**
	 * Implements to string for using ErrorMessage objects with PSR-3-logger.
	 */
	public function __toString(): string;
}