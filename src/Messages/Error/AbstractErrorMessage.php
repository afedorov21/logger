<?php
declare(strict_types=1);

namespace Sol\Logger\Messages\Error;

/**
 * Class ErrorMessage - base class for error messages
 * @package Sol\Logger\Messages\Error
 */
abstract class AbstractErrorMessage implements ErrorMessageInterface {

	/**
	 * Message text
	 * @var string
	 */
	protected $message = '';

	/**
	 * Message file
	 * @var string
	 */
	protected $file = '';

	/**
	 * Message line in file
	 * @var int
	 */
	protected $line = 0;

	/**
	 * Error trace
	 * @var string
	 */
	protected $trace = '';

	/**
	 * Error timestamp
	 * @var int
	 */
	protected $timestamp;

	/**
	 * Remote IP for this message
	 * @var string
	 */
	protected $remoteAddr = '';

	/**
	 * {@inheritdoc}
	 */
	public function getMessage(): string {
		return $this->message;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFile(): string {
		return $this->file;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getLine(): int {
		return $this->line;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTrace(): string {
		return $this->trace;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTimestamp(): int {
		return $this->timestamp;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRemoteAddr(): string {
		return $this->remoteAddr;
	}

	/**
	 * Parse default params passing for all error messages
	 * @param array $data - requestId,remoteAddr,timestamp
	 */
	protected function parseDefaultParams(array $data) {
		$this->remoteAddr = $data['remoteAddr'] ?? '';
		$this->timestamp = (!empty($data['timestamp']) && is_int($data['timestamp']) && $data['timestamp'] > 0) ? $data['timestamp'] : \time();
	}

	/**
	 * {@inheritdoc}
	 */
	public function __toString(): string {
		$buf = '';
		$buf .= \date('r', $this->getTimestamp());
		$buf .= ' - ' . $this->getRemoteAddr();
		$buf .= ' - ' . $this->getMessage()
			. " on " . $this->getFile() . ':' . $this->getLine()
			. "\nTrace: " . $this->getTrace()
			. "\n";
		return $buf;
	}
}