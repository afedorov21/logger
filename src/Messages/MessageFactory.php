<?php
declare(strict_types=1);

namespace Sol\Logger\Messages;

use Sol\Logger\Messages\Error\ExceptionMessage;

/**
 * Factory for creating messages
 * Class GateExceptionMessageFactory
 * @package Sol\Logger\Messages
 */
class MessageFactory {
	/**
	 * Create error message object for exception in gate. REMOTE_ADDR from $_SERVER;
	 * @param \Exception $message - message for creating
	 * @param array $data - data may contains: file,line,trace,remoteAddr,timestamp
	 * @return ExceptionMessage
	 */
	public function createExceptionMessage(\Exception $message, array $data = []) {
		$data['remoteAddr'] = (empty($data['remoteAddr']) && isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';
		return new ExceptionMessage($message, $data);
	}
}