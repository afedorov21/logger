<?php
declare(strict_types=1);

namespace Sol\Logger\Formatters\Error;

use Sol\Logger\Messages\Error\ErrorMessageInterface;

/**
 * Format error message to string for humans
 * Class HumanReadableFormatter
 * @package Sol\Logger\Formatters\Error
 */
class HumanReadableFormatter implements ErrorFormatterInterface {
	/**
	 * {@inheritdoc}
	 */
	public function format(string $level, ErrorMessageInterface $message): string {
		$buf = '';
		$buf .= \date('r', $message->getTimestamp());
		$buf .= ' - ';
		$buf .= $message->getRemoteAddr() ?? '~';
		$buf .= ' - ' . \php_uname('n');
		$buf .= ' - ' . $level;
		$buf .= ' - ' . $message->getMessage()
			. " on " . $message->getFile() . ':' . $message->getLine()
			. "\nTrace: " . $message->getTrace()
			. "\n";
		return $buf;
	}
}