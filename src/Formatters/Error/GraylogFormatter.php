<?php
declare(strict_types=1);

namespace Sol\Logger\Formatters\Error;

use Sol\Logger\Messages\Error\ErrorMessageInterface;

/**
 * Format error message to string for Filebeat
 * Class GraylogFormatter
 * @package Sol\Logger\Formatters\Error
 */
class GraylogFormatter implements ErrorFormatterInterface {
	/**
	 * {@inheritdoc}
	 */
	public function format(string $level, ErrorMessageInterface $message): string {
		$buf = [];
		$buf['version'] = '1.1';
		$buf['host'] = 'back';
		$buf['short_message'] = $message->getMessage();
		$buf['app__date'] = \date(DATE_RFC3339, $message->getTimestamp());
		$buf['app__remote_addr'] = $message->getRemoteAddr() ?? '~';
		$buf['app__file'] = $message->getFile();
		$buf['app__line'] = $message->getLine();
		$buf['app__level'] = $level;
		$buf['app__message'] = $message->getMessage();
		$buf['app__trace'] = $message->getTrace();
		return \json_encode($buf);
	}
}