<?php
declare(strict_types=1);

namespace Sol\Logger\Formatters\Error;

use Sol\Logger\Messages\Error\ErrorMessageInterface;

/**
 * The formatter is object for formatting messages to specific string
 * Interface ErrorFormatterInterface
 * @package Sol\Logger\Formatters\Error
 */
interface ErrorFormatterInterface {
	/**
	 * Format function - format message into string
	 * @param string $level - message level
	 * @param ErrorMessageInterface $message - message object
	 * @return string - message string representation
	 */
	public function format(string $level, ErrorMessageInterface $message): string;

}