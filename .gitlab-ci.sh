#!/usr/bin/env bash

# Install dependencies only for Docker.
[[ ! -e /.dockerinit ]] && exit 0
set -xe
